const {City} = require('./City')
const {Capital} = require('./Capital')
const {Country} = require('./Country')

let city1 = new City('Kharkiv')
let city2 = new City('Lviv')
let city3 = new City('Dnipro')
let city4 = new City('Poltava')
let city5 = new City('Sumi')
let city6 = new City('Odessa')
let city7 = new City('Donetsk')
let city8 = new City('Lugansk')
let city9 = new City('Lutsk')
let city10 = new City('Rivne')
let city11 = new City('Baku')
let myCountry=new Country({name:'Ukraine'},
cities=[{city1},{city2},{city3},{city4},{city5},{city6},{city7},{city8},{city9},{city10}])

let rem=myCountry.cities.RemoveCity([name:'Rivne'])
let add = myCountry.cities.AddCity([name:'Baku'])

function sortByTemp(cities){
cities.sort((a,b) => a.temperature > b.temperature ? 1 : -1);
}

sortByTemp(cities);

city1.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city2.setWeather().then(() => {console.log(city2)}).catch((error) => {console.log(error)})
city3.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city4.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city5.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city6.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city7.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city8.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city9.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city10.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city11.setWeather().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
let capital = new Capital('Kyiv')
capital.setAirport('Boryspil')
capital.setWeather().then(() => {console.log(capital)}).catch((error) => {console.log(error)})

